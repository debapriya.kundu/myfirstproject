package main

import "fmt"

func main() {
	var num1 float32 = 20
	var num2 float32 = -1.5
	answer := num1 / num2
	fmt.Printf("%g", answer)
}
